import java.util.Scanner;

public class LightSwitch {

	public static void main(String[] args) {
		
		Boolean light = true;    //The light starts in the on position
		Boolean broken = false;   
		String start = "plus";		//Since the light starts in the on position I have a variable set to "plus" 
									//I use this variable to determine if the light is already on.
		String input = null;		//This is the variable for the user input which I have left empty.
		
		while(!broken)				//While the switch is not broken
		{
			if(light == true)		//We will check if the light is on or off with this if statement
			{
				System.out.println("The light is on");
			}
			else										//and tell the user accordingly
			{
				System.out.println("The light is off");
			}
			
			System.out.println("Type 'minus' to turn off the light or 'plus to turn the light on.");
			Scanner in = new Scanner(System.in);				//This is where we ask the user to turn the light on or off
			input = in.nextLine();
			
			do 
			{

				if(input.equals("plus"))			//if the input equals "plus" i.e. if the light is turned on
				{
					if(start.equals(input))			//we check if the input "plus" is equal to the start variable
					{
						System.out.println("The light is already on."); //If start and the input are both plus we tell the user
						break;										//That it is already on and it starts the loop again
					}
					else 
					{
						/*If the start and input are not the same then we can be sure that the light is off
						 *  That means we can turn on the light and tell the user that it is now on. In that case
						 *  we must now set light to true and make start equal to the input so we can not turn the
						 *  light on while it is already on*/
						System.out.println("You have turned on the light.");  
						light = true;				
						start = input;
						/*Now that the light has been turned on we must check to see if the light will break.
						 * We will set a variable x to a random number between 1 and 3. */
						int x = (int) (Math.random() * 2);
						
						if(x == 1)		//if the random value for x equals one we will seat broken to true and exit the while loop.
						{				//On restarting the while loop it will see that broken is now true. Inform the user and
										//terminate the programme.
							broken = true;
						}
						break;
						
					}
				}
				/*This is pretty much the same as the when you enter "plus"
				 * it first checks if the input was "minus". If so it will check that the start variable is "minus"
				 * If its a match then it will inform the user that the light is already off. If they are not a match
				 * then we tell the user that the light is now off, set the light variable to false and set the start
				 * variable to minus so that we can not turn the light off while its off.*/
				else if (input.equals("minus"))
				{
					if(start.equals(input))
					{
						System.out.println("The light is already off.");
						break;
					}
					else 
					{
						System.out.println("You have turned off the light.");
						light = false;
						start = input;
						break;
						
					}
				}
				else	//If the user tries to enter a value that is not plus or minus we print off this and restart the loop
				{
					System.out.println("Please enter a valid value of 'plus' or 'minus'.");
					break;
				}
			} 
			while(!input.equals("plus") || !input.equals("minus"));
			

		}
		System.out.println("The light is broken. Good job."); //When the light breaks we exit the loop and print this.
		 System.exit(0);							//The programme then terminates itself
		
		
		
	}

}

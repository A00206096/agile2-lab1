import junit.framework.TestCase;


public class AnalyseStringTest extends TestCase {
	
	//Test#: 001
	//Objective: get length of string
	//input: Conor
	//Expected output: 5
	
	public void testgetStringlen()
	{
		assertEquals(5, AnalyseString.getString("Conor"));
	}
	
	//Test#: 002
	//Objective: get number of vowels of a string
	//input: Conor
	//Expected output: 2
	
	public void testgetVowels()
	{
		assertEquals(2, AnalyseString.getVowels("Conor"));
	}
	
	//Test#: 003
	//Objective: get first letter of a string
	//input: Conor
	//Expected output: C
	
	public void testgetFirstletter()
	{
		assertEquals('C', AnalyseString.getFirstletter("Conor"));
	}
	
	//Test#: 004
	//Objective: get last letter of a string
	//input: Conor
	//Expected output: r
	
	public void testgetLastLetter()
	{
		assertEquals('r', AnalyseString.getLastLetter("Conor"));
	}
}


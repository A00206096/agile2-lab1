import java.util.Scanner;

public class Box
{

	public static void main(String[] args) 
	{
		int w = 0;
		int l = 0;
		int h = 0;
		
		Scanner in = new Scanner (System.in);
		System.out.println("Please enter a width: ");
		w = in.nextInt();
		System.out.println("Please enter a height: ");
		h = in.nextInt();
		System.out.println("Please enter a length: ");
		l = in.nextInt();
		
		System.out.println("The area of the box is : " + getArea(w, h));
		System.out.println("The volume of the box is : " + getVolume(w, l, h));
		
	}
		
	static int getArea(int w, int h)
	{
		int Area = (w * h) * 6;
		return Area;
	}
	
	static int getVolume(int w, int l, int h)
	{
		int Volume = (w * l * h);
		return Volume;
	}
}



import junit.framework.TestCase;

public class BoxTest extends TestCase {
	
	/*Test: 001
	 *Objective: get the area 
	 *Inputs: 2, 2, 2
	 *expected output: 48*/
	
	public void getAreaTest()
	{
		assertEquals(24, Box.getArea(2, 2));
	}
	
	/*Test: 002
	 * Objective: get the volume
	 * Input: 2, 2, 2
	 * expected output: 8*/
	public void getVolumeTest()
	{
		assertEquals(8, Box.getVolume(2, 2, 2));
	}
}

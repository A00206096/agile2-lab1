import java.util.Scanner;

public class AnalyseString {
	
	static int getString(String str)
	{
		return str.length();	
	}
	
	static int getVowels(String str)
	{
		int vowels = 0;
		
		for(int i = 0; i < str.length(); i++)
		{
			if(str.charAt(i) == 'a' || str.charAt(i) == 'A' || str.charAt(i) == 'e' || str.charAt(i) == 'E'
					|| str.charAt(i) == 'i' || str.charAt(i) == 'I' || str.charAt(i) == 'o' || str.charAt(i) == 'O'
					|| str.charAt(i) == 'u' || str.charAt(i) == 'U')
			{
				vowels ++;
			}
		}
		return vowels;
	}
	
	static char getFirstletter(String str)
	{
		return str.charAt(0);
	}
	
	static char getLastLetter(String str)
	{
		return str.charAt(str.length() - 1);
	}
	
	public static void main (String [] args)
	{
		System.out.println("Please enter a word: ");
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		
		System.out.println("The length of the string is : " + getString(input));
		System.out.println("The first letter of the string is : " + getFirstletter(input));
		System.out.println("The last letter of the string is : " + getLastLetter(input));
		System.out.println("The number of vowels in the string is : " + getVowels(input));
	}
}
